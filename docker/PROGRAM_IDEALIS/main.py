import RPi.GPIO as GPIO
import time
import urllib.request

GPIO.setwarnings(False)

#================================================================

print ("")
print("program untuk menghitung nyala LED oleh LDR")
while True:
    hitungan = 0
    waktu_tidur = 0.5
    led_sudah_nyala = 0
    led_suruh_nyala = 10 # Diubah untuk memanipulasi nyala led
    GPIO.setmode(GPIO.BOARD)
    GPIO.setup(11, GPIO.IN)
    tombol = GPIO.input(11)
    if tombol == False:
        print ("")
        print ('tombol ditekan')
        while led_sudah_nyala < led_suruh_nyala :
            led_sudah_nyala = led_sudah_nyala + 1
            GPIO.setup(36, GPIO.OUT)
            GPIO.output(36, GPIO.HIGH)
            time.sleep(waktu_tidur)
            GPIO.cleanup()
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(24, GPIO.IN)
            if GPIO.input(24) == GPIO.LOW :
                time.sleep (waktu_tidur)
                hitungan = hitungan + 1             
                print ('hitungan bertambah menjadi =', hitungan, "hitungan sekarang", hitungan)
                GPIO.cleanup()
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(36, GPIO.OUT)
                GPIO.output(36, GPIO.LOW)
                time.sleep(waktu_tidur)
            elif GPIO.input(24)  == GPIO.HIGH :
                time.sleep (waktu_tidur)
                hitungan = ((hitungan + 1) - 1)
                print ('hitungan tidak bertambah', "      hitungan sekarang", hitungan)
                GPIO.cleanup()
                GPIO.setmode(GPIO.BOARD)
                GPIO.setup(36, GPIO.OUT)
                GPIO.output(36, GPIO.LOW)
                time.sleep(waktu_tidur)
            else:
                pass            
        with urllib.request.urlopen('https://www.monitoring.firmandev.tech/index.php/data?set=YMukbp35unN7Emmoj0SfI4RhRoiTGlMJ&log=LDR%20mendeteksi%20nyala%20led%20' + str(hitungan) + '%20kali') as response:
            if response.getcode() == 200:
                print('Data Terkirim')
            else:
                pass            
    if tombol == True:
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(36, GPIO.OUT)
        GPIO.output(36, GPIO.LOW)
        time.sleep (0.5)
    else:
        pass
    GPIO.cleanup
