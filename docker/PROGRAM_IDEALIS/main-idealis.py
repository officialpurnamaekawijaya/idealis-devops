

#==================================================================================================================================================
#class bmimeasuringsystem
class bmimeasuringsystem :
    def generate_bmi(self, height, weight):
        if self.error == 0 :
            print ('===========class bmimeasuringsystem.generate_bmi===========')
            self.height = tinggi
            self.rfid_id = rfid_id
            self.card_id = card_id
            self.weight = berat
            self.bmi    = round(berat/(tinggimeter * tinggimeter), 2)            
            print ('NILAI BMI ANDA ADALAH    = ',self.bmi) #,self.bmi
            print ('-----------------------------------------------------------')
            print ('KATEGORI AMBANG BATAS ANDA ADALAH  ')
            if self.bmi <= 17 :
                print ('KURUS, KEKURANGAN BERAT BADAN TINGKAT BERAT')
                ab = str('KURUS, T.BERAT')
                self.ab = 1
            elif  17 < self.bmi <= 18.5 :
                print ('KURUS, KEKURANGAN BERAT BADAN TINGKAT RINGAN')
                ab = str('KURUS, T.RINGAN')
                self.ab = 2
            elif 18.5 < self.bmi <= 25 :
                print ('NORMAL')
                ab = 'NORMAL'
                self.ab = 3
            elif 25 < self.bmi <= 27 :
                print ('GEMUK, KELEBIHAN BERAT BADAN TINGKAT RINGAN')
                ab = 'GEMUK, T.RINGAN'
                self.ab = 4
            elif self.bmi > 27 :
                print ('GEMUK, KELEBIHAN BERAT BADAN TINGKAT BERAT')
                ab = 'GEMUK, T.BERAT'
                self.ab = 5
            else :
                print('KATEGORI AMBANG BATAS BMI ANDA TIDAK DI KETAHUI')
            print ('-----------------------------------------------------------')
            beratidealbawah = round(tinggimeter * tinggimeter * 18.5 , 2)
            self.bib = beratidealbawah
            beratidealatas = round(tinggimeter * tinggimeter * 25 , 2)
            self.bia = beratidealatas
            print ('BERAT BADAN IDEAL ANDA ADALAH', beratidealbawah,' KG SAMPAI', beratidealatas,' KG')
            print ('-----------------------------------------------------------')
            if self.weight < beratidealbawah :
                tambahberatbawah = round(beratidealbawah - berat , 2)
                self.tbb = tambahberatbawah
                tambahberatatas = round(beratidealatas - berat , 2)
                self.tba = tambahberatatas
                self.kbb = 0.00
                self.kba = 0.00
                print ('TAMBAH BERAT BADAN ANDA ANTARA', tambahberatbawah,' KG  SAMPAI', tambahberatatas,' KG')
            elif self.weight > beratidealatas :
                kurangiberatbawah = round(berat - beratidealatas , 2)
                self.kbb = kurangiberatbawah
                kurangiberatatas =round( berat - beratidealbawah ,2)
                self.kba = kurangiberatatas
                self.tbb = 0.00
                self.tba = 0.00
                print ('KURANGI BERAT BADAN ANDA ANTARA', kurangiberatbawah,' KG SAMPAI', kurangiberatatas,' KG')
            elif beratidealbawah < self.weight < beratidealatas :
                self.tbb = 0.00
                self.tba = 0.00
                self.kbb = 0.00
                self.kba = 0.00
                print ('BERAT BADAN ANDA NORMAL')
                print ('ANDA TIDAK PERLU MENGURANGI ATAU MENAMBAH BERAT BADAN ANDA')
            else :
                print('BERAT BADAN ANDA ANEH')
            print ('-----------------------------------------------------------')
            print ('===========================  -  ===========================')
        elif self.error == 1 :
            pass
        elif self.error == 2 :
            pass
        else :
            pass
	
    def save_record_to_database (self, height, weight):
        if self.error == 0 :
            self.height = tinggi
            self.weight = berat
            with urllib.request.urlopen('https://www.idealis.firmandev.tech?access_key=EEOqsRfRR2Q0E5R7CVMZrSNwPCyXP8jj&insert&waktu=0&rfid=' + str(self.card_id) + '&tb=' + str(self.height) + '&bb=' + str(self.weight) + '&bmi=' + str(self.bmi) + '&ab=' + str(self.ab) + '&bia=' + str(self.bia) + '&bib=' + str(self.bib) + '&tba=' + str(self.tba) + '&tbb=' + str(self.tbb) + '&kba=' + str(self.kba) + '&kbb=' + str(self.kbb)) as response:
                if response.getcode() == 200:
                    print("Data BMI anda telah tersimpan ke database perangkat")
                else:
                    print("Error: Data BMI anda belum tersimpan ke database perangkat")
            tampilan_utama()
        elif self.error == 1 :
            pass
        elif self.error == 2 :
            pass
        else :
            pass

    def identification(self):
        if ldr == 0 :
            self.error = 0
            print("ERROR = 0")
            print("")
        else:
            self.error = 1
            print("ERROR = 1")
            print("")
#==================================================================================================================================================
#libraries
import RPi.GPIO as GPIO
import time
from time import sleep
import urllib.request
import json
#==================================================================================================================================================
def tampilan_utama() :
    print('')
    print ('===========================================================')
    print ('-----------------SELAMAT DATANG DI IDEALIS-----------------')
    print ('===========================================================')
    print ('      ALAT PENGUKUR DAN PENYIMPAN DATA BMI OTOMATIS        ')
    print ('===========================================================')
    print('')
    print ('    SILAHKAN MENEMPATKAN DIRI DI ANTARA MAKET SISTEM')
    print('            DAN MELAKUKAN PENGENALAN IDENTITAS')
    print('')
#main program
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(36, GPIO.OUT, initial=GPIO.LOW)#set pin 8 sebagai output dan set nilai inisial = off
GPIO.output(36, GPIO.HIGH)
sleep(0.5)
GPIO.output(36, GPIO.LOW)
sleep(0.5)
GPIO.output(36, GPIO.HIGH)
sleep(0.5)
GPIO.output(36, GPIO.LOW)
sleep(0.5)
GPIO.output(36, GPIO.HIGH)
sleep(0.5)
GPIO.output(36, GPIO.LOW)
tampilan_utama()

jsond = ''
with urllib.request.urlopen('https://idealis.firmandev.tech?access_key=EEOqsRfRR2Q0E5R7CVMZrSNwPCyXP8jj&get_sensor_private') as response:
    if response.getcode() == 200:
        jsond = response.read()
    else:
        pass
y = json.loads(jsond.decode("utf-8"))
tmp_token = y['token']

while True :
    jsond = ''
    with urllib.request.urlopen('https://idealis.firmandev.tech?access_key=EEOqsRfRR2Q0E5R7CVMZrSNwPCyXP8jj&get_sensor_private') as response:
        if response.getcode() == 200:
            jsond = response.read()
        else:
            pass
    y = json.loads(jsond.decode("utf-8"))

    if(tmp_token != y['token']):
        tmp_token = y['token']
        
        print ('')
        rfid_id = y['rfid']
        card_id = rfid_id
        tinggi = int(y['height'])
        berat = int(y['weight'])
        ldr = int(y['ldr'])
        tinggimeter = tinggi / 100
        print('')
        obj1 = bmimeasuringsystem()
        obj1.identification()
        obj1.generate_bmi(tinggimeter, berat)
        obj1.save_record_to_database(tinggi, berat)
#==================================================================================================================================================
